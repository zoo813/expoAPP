import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, Button, Platform, StyleSheet, SafeAreaView, StatusBar, TouchableOpacity, TouchableHighlight } from 'react-native';
import { WebView } from 'react-native-webview';
import { AntDesign, FontAwesome, Ionicons } from '@expo/vector-icons';

const WEBVIEW_URL =  "https://velog.io/@happymandoo/React-Native-Expo-%EC%85%8B%ED%8C%85" ;
// const WEBVIEW_URL =  "https://naver.com" ;

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const webview = useRef();
  const [navState, setNavState] = useState();

  useEffect(() => {
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  return (
    // <View
    //   style={{
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: 'space-around',
    //   }}
    // >
    //   <WebView source={{ uri: "https://naver.com" }} style={{ marginTop: 10 }} />
    //   <WebView 
    //     allowsFullscreenVideo // Youtube FullScreen
    //     textZoom={100} //Erratic TextSize
    //     javaScriptEnabled={true} //JS Support
    //     domStorageEnabled={true} //Cache
    //     // ref={(webView) => { this.webView.ref = webView; }} 
    //     source={{ uri: "https://naver.com" }} 
    //     />
    //   <Text>Your expo push token: {expoPushToken}</Text>
    //   <View style={{ alignItems: 'center', justifyContent: 'center' }}>
    //     <Text>Title: {notification && notification.request.content.title} </Text>
    //     <Text>Body: {notification && notification.request.content.body}</Text>
    //     <Text>Data: {notification && JSON.stringify(notification.request.content.data)}</Text>
    //   </View>
      
    //   <Button
    //     title="Press to schedule a notification"
    //     onPress={async () => {
    //       await schedulePushNotification();
    //     }}
    //   />
    // </View>
    <View style={styles.container}>

      <WebView
        ref = {webview}
        source={{uri: WEBVIEW_URL}}
        style={{marginTop: 20}}
        useWebKit={true}
        onNavigationStateChange={setNavState}
        // allowsBackForwardNavigationGestures={true}
        />
      {/* <Button
        title="Press to schedule a notification"
        color="#f194ff"
        background ="#f194ff"
        onPress={async () => {
          await schedulePushNotification();
        }}
      /> */}
      <TouchableHighlight activeOpacity={0.8} style={styles.button} 
        onPress={async () => {
          await schedulePushNotification();
        }}>
        <Text style={styles.text}>푸시 알림 버튼</Text>
      </TouchableHighlight>
      <TouchableHighlight underlayColor="rgba(0,0,0,0)"
        style={styles.backBtn} onPress={() => {
          if(navState.canGoBack) {
            // 뒤로 갈 수 있는 상태라면 이전 웹페이지로 이동한다
            webview.current.goBack();
    
          } else {
            // 뒤로 갈 수 없는 상태라면
            // 다른 원하는 행동을 하면 된다
            console.log("back");
          }
        }}>
          <View><AntDesign name="arrowleft" size ={25} color="#fff" /></View>
      </TouchableHighlight>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch', // 이부분 중요 이거 안써주면 WebView 에 width 값을 지정해야함.. 
    justifyContent: 'center',
    // paddingTop: getStatusBarHeight(true),
  },
  button: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 60,
    backgroundColor: "#ddf",
    justifyContent: "center",
    alignItems: "center"
  },
  backBtn:{
    position:"absolute",
    bottom:20,
    left: 20,
    width: 30,
    // backgroundColor:"#ddf",
    paddingVertical:2,
    borderRadius:50
  }
});

async function schedulePushNotification() {
  await Notifications.scheduleNotificationAsync({
    content: {
      title: "만두 PUSH~_~!",
      body: '푸시 알림 테스트입니다.',
      data: { data: 'goes here' },
    },
    trigger: { seconds: 5 },
  });
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}